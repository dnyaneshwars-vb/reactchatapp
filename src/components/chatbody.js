import React, { PureComponent } from "react";
import ChatList from "./ChatList";
import ChatSend from "./ChatSend";

class ChatBody extends PureComponent {
    constructor(props) {
        super(props);
        


        this.state = {
            data : [
                {
                    message : "hi",
                    createdAt : "2013-07-13T14:35:00Z",
                    sender:"agent",
                    _id : 1
                },
                {
                    message : "how r u doin today",
                    createdAt : "2013-07-13T14:35:00Z",
                    sender:"user",
                    _id : 2
                },
                {
                    message : "i am good, how about you",
                    createdAt : "2013-07-13T14:35:00Z",
                    sender:"agent",
                    _id : 3
                },
                {
                    message : "i am fine. thanks for asking",
                    createdAt : "2013-07-13T14:35:00Z",
                    sender:"user",
                    _id : 4
                },
                {
                    message : "how can i help you today",
                    createdAt : "2013-07-13T14:35:00Z",
                    sender:"agent",
                    _id : 5
                }
            ]   
        }

        this.getData = this.getData.bind(this);
        this.update = this.update.bind(this);
        this.onNameChange = this.onNameChange.bind(this);
    }
    
    update(){
        console.log("hi");
        console.log(this.state.data);
        this.setState({});
        this.onNameChange();
    }
    
    
    getData(val){
        
        var pp = {
            message : val,
            createdAt : "2013-07-13T14:35:00Z",
            sender:"user",
            _id : 6
        }
        this.setState({
            data: this.state.data.concat(pp)
        });
        if(this.state.data.length > 5){
            this.update();
        }
    }
    onNameChange(){
        this.setState({});
    }

    render() { 
        
        return ( 
            <div className="chatbody">
                <ChatList name={this.state} onChange={this.onNameChange} />
                <ChatSend sendData={this.getData} />
            </div>
         );
    }
}
 
export default ChatBody;