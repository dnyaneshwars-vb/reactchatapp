import React, { PureComponent } from "react";

class ChatSend extends PureComponent {
    constructor(props) {
        super(props);
        this.state={
            message : ""
        }

        this.onChangeValue = this.onChangeValue.bind(this);
        this.onSubmit = this.onSubmit.bind(this);
        
    }
    onChangeValue(e){
        var a = e.target.value;
        this.setState({
            message:a
        })
    }
    onSubmit(e) {
        e.preventDefault();
        console.log(this.state);
        this.props.sendData(this.state.message);
        this.setState({
            message:""
        })
        
    }
    render() { 
        return ( 
            <div className="chatsend">
                <form action="">
                    <input type="text" value={this.state.message} onChange={this.onChangeValue} id="" />
                    <input type="button" value="send"  onClick={this.onSubmit} />   
                </form> 
            </div>
         );
    }
}
 
export default ChatSend;