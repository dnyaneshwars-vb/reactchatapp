import './App.css';
import ChatBody from "./components/chatbody";

function App() {
  return (
    <div className="App">
     <ChatBody />
    </div>
  );
}

export default App;
